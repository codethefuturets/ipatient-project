
//
//  RehabTableViewController.m
//  iPatient
//
//  Created by Jeremy Kulchyk on 2016-05-18.
//  Copyright © 2016 Kulchapps. All rights reserved.
//

#import "RehabTableViewController.h"
#import "RRehabTableViewCell.h"

@interface RehabTableViewController ()

@end

@implementation RehabTableViewController


NSString *urlStringg0 = @"https://www.youtube.com/watch?v=zqsWGmXR5xs";
NSString *urlStringg1 = @"https://www.youtube.com/watch?v=FzCXI_mTuhc";
NSString *urlStringg2 = @"https://www.youtube.com/watch?v=jR53sAe67SU";
NSString *urlStringg3 = @"https://www.youtube.com/watch?v=OJ-VI39sLgA";
NSString *urlStringg4 = @"https://www.youtube.com/watch?v=19O0Cb0WxYM";
NSString *urlStringg5 = @"https://www.youtube.com/watch?v=jR53sAe67SU";
NSString *urlStringg6 = @"https://www.youtube.com/watch?v=I4FyvyO-0kU";
NSString *urlStringg7 = @"https://www.youtube.com/watch?v=pf7SO-l2QTU";
NSString *urlStringg8 = @"https://www.youtube.com/watch?v=l6kOD4qCtR8";
NSString *urlStringg9 = @"https://www.youtube.com/watch?v=GJDMUmabrZk";
NSString *urlStringg10 = @"https://www.youtube.com/watch?v=7QtIKRIi7ow";
NSString *urlStringg11 = @"https://www.youtube.com/watch?v=MC6aa4fq-XE";
NSString *urlStringg12 = @"https://www.youtube.com/watch?v=sUEyNXgKdpo";
NSString *urlStringg13 = @"https://www.youtube.com/watch?v=Sd7uDkSaGS4";
NSString *urlStringg14 = @"https://www.youtube.com/watch?v=ouYjWeePqmQ";
NSString *urlStringg15 = @"https://www.youtube.com/watch?v=zBq6osSwTiE";
NSString *urlStringg16 = @"https://www.youtube.com/watch?v=ycFRIetrGxw";
NSString *urlStringg17 = @"https://www.youtube.com/watch?v=7NMkFphpXsg";
NSString *urlStringg18 = @"https://www.youtube.com/watch?v=LTK752aTtBk";
NSString *urlStringg19 = @"https://www.youtube.com/watch?v=KtvKrF312jg";
NSString *urlStringg20 = @"https://www.youtube.com/watch?v=uGPEJEAoWjg";
NSString *urlStringg21 = @"https://www.youtube.com/watch?v=ChDP4rG1vdA";
NSString *urlStringg22 = @"https://www.youtube.com/watch?v=ctIuNeov__s";
NSString *urlStringg23 = @"https://www.youtube.com/watch?v=qVUuJ6xf-bw";
NSString *urlStringg24 = @"https://www.youtube.com/watch?v=P3HH1qrAADQ";
NSString *urlStringg25 = @"https://www.youtube.com/watch?v=s8dS26inRPg";
NSString *urlStringg26 = @"https://www.youtube.com/watch?v=FnkobQrvI-o";
NSString *urlStringg27 = @"https://www.youtube.com/watch?v=4TbUBGvTSWM";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    _Stretch = @{@"Back" : @[@"Sphinx progression 1",
                             @"Sphinx progression 2",
                             @"Sphinx progression 3",
                             @"One legged flexion",
                             @"Two legged flexion",
                             @"Resistance band rows",
                             @"Thoracic rotation",
                             @"Cat-Camel",
                             @"Bird Dog"],
                 @"Hip" : @[@"Glute side bridge",
                            @"Isometric glute contraction",
                            @"Foam roller",
                            @"Hip flexor stretch",
                            @"Glute bridge",
                            @"Posterior pelvic tilt"],
                 @"Ankle/foot" : @[@"Toe raises",
                                   @"Towel scrunch",
                                   @"Myofascial release: ball",
                                   @"Myofascial release: foot/calf",
                                   @"Ankle motion",
                                   @"Ankle motion: resistance",
                                   @"Short foot pronation",
                                   @"One foot balancing"],
                 @"Core" : @[@"Dead bug",
                             @"Planks: front/side"],
                 @"Wrist" : @[@"Intrinsic muscle: scissors",
                              @"Wrist warm up",
                              @"Wrist motion"],
                @"Shoulder" : @[@"test1"],
                @"Elbow" : @[@"test2"]};
    
    _Sections = [[_Stretch allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
}



- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    return [_Sections objectAtIndex:section];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return [_Sections count];}
    
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
        
        // Return the number of rows in the section.
    
    NSString *sectionTitle = [_Sections objectAtIndex:section];
    NSArray *sectionAnimals = [_Stretch objectForKey:sectionTitle];
    return [sectionAnimals count];

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        /*static NSString *CellIdentifier = @"RehabTableViewCell";
        RRehabTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        // Configure the cell...
        int row = [indexPath row];
    
        cell.StretchLabel.text = _Back[row];
        
        return cell;*/
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RehabTableViewCell" forIndexPath:indexPath];
    
    // Configure the cell...
    NSString *sectionTitle = [_Sections objectAtIndex:indexPath.section];
    NSArray *sectionAnimals = [_Stretch objectForKey:sectionTitle];
    NSString *animal = [sectionAnimals objectAtIndex:indexPath.row];
    cell.textLabel.text = animal;
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
        
        switch (indexPath.row) {
            case 0:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg0]];
                break;
            case 1:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg1]];
                break;
            case 2:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg2]];
                break;
            case 3:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg3]];
                break;
            case 4:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg4]];
                break;
            case 5:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg5]];
                break;
            case 6:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg6]];
                break;
            case 7:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg7]];
                break;
            case 8:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg8]];
                break;
            case 9:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg8]];
                break;
            case 10:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg10]];
                break;
            case 11:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg11]];
                break;
            case 12:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg12]];
                break;
            case 13:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg13]];
                break;
            case 14:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg14]];
                break;
            case 15:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg15]];
                break;
            case 16:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg16]];
                break;
            case 17:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg17]];
                break;
            case 18:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg18]];
                break;
            case 19:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg19]];
                break;
            case 20:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg20]];
                break;
            case 21:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg21]];
                break;
            case 22:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg22]];
                break;
            case 23:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg23]];
                break;
            case 24:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg24]];
                break;
            case 25:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg25]];
                break;
            case 26:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg26]];
                break;
            case 27:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg27]];
                break;
            case 28:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg27]];
                break;
            case 29:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStringg27]];
                break;
                default:
                break;
    
        }}
@end

