//
//  HomeTableViewCell.h
//  iPatient
//
//  Created by Jeremy Kulchyk on 2016-06-24.
//  Copyright © 2016 Kulchapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeTableViewCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UILabel *StretchLabel;
@property (nonatomic,strong) IBOutlet UILabel *TimeLabel;
//@property (nonatomic,strong) IBOutlet UIButton *Complete;

@end
