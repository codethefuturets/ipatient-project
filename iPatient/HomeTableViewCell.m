//
//  HomeTableViewCell.m
//  iPatient
//
//  Created by Jeremy Kulchyk on 2016-06-24.
//  Copyright © 2016 Kulchapps. All rights reserved.
//

#import "HomeTableViewCell.h"

@implementation HomeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
