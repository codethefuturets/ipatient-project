//
//  RRehabTableViewCell.m
//  iPatient
//
//  Created by Jeremy Kulchyk on 2016-05-18.
//  Copyright © 2016 Kulchapps. All rights reserved.
//

#import "RRehabTableViewCell.h"

@implementation RRehabTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
