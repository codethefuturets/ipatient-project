//
//  main.m
//  iPatient
//
//  Created by Jeremy Kulchyk on 2016-05-02.
//  Copyright © 2016 Kulchapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
