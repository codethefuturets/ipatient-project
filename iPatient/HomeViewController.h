//
//  HomeViewController.h
//  iPatient
//
//  Created by Jeremy Kulchyk on 2016-06-16.
//  Copyright © 2016 Kulchapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeTableViewCell.h"


NSDate *today;
int daysToAdd;
NSDate *newdate;
NSArray *StretchNames;
NSArray *Times;
NSMutableArray *tableArray;

int cell1;
int cell2;
int cell3;
int cell4;
int cell5;
int routine;




@interface HomeViewController : UIViewController 

{
    IBOutlet UILabel *Date1;
    IBOutlet UIButton *RightDate;
    IBOutlet UIButton *LeftDate;
    IBOutlet UITableView *table1;

    
}



-(IBAction)RightDate:(id)sender;
-(IBAction)LeftDate:(id)sender;

@end
