//
//  HomeViewController.m
//  iPatient
//
//  Created by Jeremy Kulchyk on 2016-06-16.
//  Copyright © 2016 Kulchapps. All rights reserved.
//

#import "HomeViewController.h"
#import "HomeTableViewCell.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    daysToAdd = 0;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyyMMdd";
    today = [NSDate date];
    
    dateFormatter.dateFormat=@"MMMM";
    NSString *monthString = [[dateFormatter stringFromDate:today] capitalizedString];
    
    dateFormatter.dateFormat=@"EEEE";
    NSString *dayString = [[dateFormatter stringFromDate:today] capitalizedString];
    
    dateFormatter.dateFormat = @"dd";
    NSString *daynumber = [[dateFormatter stringFromDate:today] capitalizedString];
    
    Date1.text = [NSString stringWithFormat:@"%@, %@ %@",dayString,monthString,daynumber];
    
    
    StretchNames = @[@"Sphinx progression 1",@"Sphinx Progression 2",@"Sphinx Progression3",@"One Legged flexion",@"Two Legged Flexion",@"Resistance Band Rows",@"Thoracic Rotation",@"Cat-Camel",@"Bird dog",@"Glute side bridge",@"Isometric glute contraction",@"Foam roller",@"Hip flexor stretch",@"Glute bridge",@"Posterior pelvic tilt",@"Toe raises",@"Towel scrunch",@"Myofascial release: ball",@"Myofascial release: foot/calf",@"Ankle motion",@"Ankle motion: resistance",@"Short foot pronation",@"One foot balancing",@"Dead bug",@"Planks: front/side",@"Intrinsic muscle: scissors",@"Wrist warm up",@"Wrist motion"];
    
    Times = @[@"8:30 am",@"10:30 pm",@"7:00 pm",@"9:00 pm"];
    
    routine =0;
    cell1 = routine;
    cell2 = routine + 5;
    cell3 = routine +10;
    cell4 = routine + 15;
    cell5 = routine + 20;
    
    
    tableArray = @[StretchNames[cell1],StretchNames[cell2],StretchNames[cell3],StretchNames[cell4]];

}

-(IBAction)RightDate:(id)sender{

    
    daysToAdd++;
    
    NSDate *newdate = [NSDate dateWithTimeInterval:(24*60*60*daysToAdd) sinceDate:[NSDate date]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyyMMdd";
    today = [NSDate date];
    
    dateFormatter.dateFormat=@"MMMM";
    NSString *monthString = [[dateFormatter stringFromDate:newdate] capitalizedString];
    
    dateFormatter.dateFormat=@"EEEE";
    NSString *dayString = [[dateFormatter stringFromDate:newdate] capitalizedString];
    
    dateFormatter.dateFormat = @"dd";
    NSString *daynumber = [[dateFormatter stringFromDate:newdate] capitalizedString];
    
    
    
    Date1.text = [NSString stringWithFormat:@"%@, %@ %@",dayString,monthString,daynumber];
   
    if (routine == 4){
        routine = 0;
    }
    else{
        routine++;
    }
    
    cell1 = routine;
    cell2 = routine + 5;
    cell3 = routine +10;
    cell4 = routine + 15;
    cell5 = routine + 20;
    
    
    tableArray = @[StretchNames[cell1],StretchNames[cell2],StretchNames[cell3],StretchNames[cell4]];
    
    [table1 reloadData];
    
    
}
//
-(IBAction)LeftDate:(id)sender{
    daysToAdd--;
    
    NSDate *newdate = [NSDate dateWithTimeInterval:(24*60*60*daysToAdd) sinceDate:[NSDate date]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyyMMdd";
    today = [NSDate date];
    
    dateFormatter.dateFormat=@"MMMM";
    NSString *monthString = [[dateFormatter stringFromDate:newdate] capitalizedString];
    
    dateFormatter.dateFormat=@"EEEE";
    NSString *dayString = [[dateFormatter stringFromDate:newdate] capitalizedString];
    
    dateFormatter.dateFormat = @"dd";
    NSString *daynumber = [[dateFormatter stringFromDate:newdate] capitalizedString];
    
    Date1.text = [NSString stringWithFormat:@"%@, %@ %@",dayString,monthString,daynumber];
    
    if (routine == 0){
        routine = 4;
    }
    else{
        routine--;
    }
    cell1 = routine;
    cell2 = routine + 5;
    cell3 = routine +10;
    cell4 = routine + 15;
    cell5 = routine + 20;
    
    
    tableArray = @[StretchNames[cell1],StretchNames[cell2],StretchNames[cell3],StretchNames[cell4]];
    
    [table1 reloadData];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [Times count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    HomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HomeTableViewCell" forIndexPath:indexPath];

    NSString *StretchName = [tableArray objectAtIndex:indexPath.row];
    cell.StretchLabel.text = StretchName;
    
    NSString *TimeName = [Times objectAtIndex:indexPath.row];
    cell.TimeLabel.text = TimeName;

    return cell;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
